:experimental:

== Editing a board

=== Placement and drawing operations

Placement and drawing tools are located in the right toolbar.  When a tool is activated, it stays
active until a different tool is selected or the tool is canceled with the kbd:[Esc] key.  The
selection tool is always activated when any other tool is canceled.

Some toolbar buttons have more than one tool available in a palette.  These tools are indicated
with a small arrow in the lower-right corner of the button:
image:images/pcbnew_palette_buttons.png[]

To show the palette, you can click and hold the mouse button on the tool or click and drag the
mouse.  The palette will show the most recently used tool when it is closed.

[width="100%",cols="5%,95%",]
|====
| image:images/icons/cursor.png[]
    | Selection tool (the default tool).
| image:images/icons/tool_ratsnest.png[]
    | Local ratsnest tool: when the board ratsnest is hidden, selecting footprints with this tool
      will show the ratsnest for the selected footprint only.  Selecting the same footprint again
      will hide its ratsnest.  The local ratsnest setting for each footprint will remain in effect
      even after the local ratsnest tool is no longer active.
| image:images/icons/module.png[]
    | Footprint placement tool: click on the board to open the footprint chooser, then click again
      after choosing a footprint to confirm its location.
| image:images/icons/add_tracks.png[]

  image:images/icons/ps_diff_pair.png[]
    | Route tracks / route differential pairs: These tools activate the interactive router and
      allow placing tracks and vias.  The interactive router is described in more detail in the
      Routing Tracks section below.
| image:images/icons/ps_tune_length.png[]

  image:images/icons/ps_diff_pair_tune_length.png[]

  image:images/icons/ps_diff_pair_tune_phase.png[]
    | Tune length: These tools allow you to tune the length of single tracks or the length or
    skew of differential pairs, after they have been routed.  See the Routing Tracks section for
    details.
| image:images/icons/add_via.png[]
    | Add vias: allows placing vias without routing tracks.

    Vias placed on top of tracks using this tool will take on the net of the closest track segment
    and will become part of that track (the via net will be updated if the pads connected to the
    tracks are updated).

    Vias placed anywhere else will take on the net of a copper zone at that location, if one
    exists.  These vias will not automatically take on a new net if the net of the copper zone is
    changed.
| image:images/icons/add_zone.png[]
    | Add filled zone: Click to set the start point of a zone, then configure its properties before
      drawing the rest of the zone outline.  Zone properties are described in more detail below.
| image:images/icons/add_keepout_area.png[]
    | Add rule area: Rule areas, formerly known as keepouts, can restrict the placement of items
      and the filling of zones and can also define named areas to apply specific custom design
      rules to.
| image:images/icons/add_line.png[]
    | Draw lines.

    *Note:* Lines are graphical objects and are not the same as tracks placed with the Route Tracks
          tool.  Graphical objects cannot be assigned to a net.
| image:images/icons/add_arc.png[]
    | Draw arcs: pick the center point of the arc, then the start and end points.
| image:images/icons/add_rectangle.png[]
    | Draw rectangles.  Rectangles can be filled or outlines.
| image:images/icons/add_circle.png[]
    | Draw circles.  Circles can be filled or outlines.
| image:images/icons/add_graphical_polygon.png[]
    | Draw graphical polygons.  Polygons can be filled our outlines.

    *Note:* Filled graphical polygons are not the same as filled zones: graphical polygons cannot
    be assigned to a net and will not keep clearance from other items.
| image:images/icons/text_24.png[]
    | Add text.
| image:images/icons/add_aligned_dimension.png[]

  image:images/icons/add_orthogonal_dimension.png[]

  image:images/icons/add_center_dimension.png[]

  image:images/icons/add_leader.png[]
    | Add dimensions.  Dimension types are described in more detail below.
| image:images/icons/add_pcb_target.png[]
    | Add layer alignment mark.
| image:images/icons/delete.png[]
    | Deletion tool: click objects to delete them.
| image:images/icons/set_origin.png[]
    | Set drill/place origin.  Used for fabrication outputs.
| image:images/icons/grid_select_axis.png[]
    | Set grid origin.

|====

=== Snapping

When moving, dragging, and drawing board elements, the grid, pads, and other elements can have
snapping points depending upon the settings in the user preferences.  In complex designs, snap
points can be so close together that it makes the current tool action difficult.  Both grid and
object snapping can be disabled while moving the mouse by using the modifier keys in the table
below.

[options="header",cols="40%,60%"]
|====
| Modifier Key | Effect
| kbd:[Ctrl] | Disable grid snapping.
| kbd:[Shift] | Disable object snapping.
|====

=== Editing object properties

All objects have properties that are editable in a dialog.  Use the hotkey kbd:[E] or select
Properties from the right-click context menu to edit the properties of selected item(s).  You can
only open the properties dialog if all the items you have selected are of the same type.  To edit
the properties of different types of items at one time, see the section below on bulk editing
tools.

In properties dialogs, any field that contains a numeric value can also accept a basic math
expression that results in a numeric value.  For example, a dimension may be entered as `2 * 2mm`,
resulting in a value of `4mm`.  Basic arithmetic operators as well as parentheses for defining
order of operations are supported.

=== Working with footprints

NOTE: TODO: Write this section - covers footprint properties, updating from library, etc.

=== Working with pads

NOTE: TODO: Write this section - covers pad properties

=== Working with zones

NOTE: TODO: Write this section

=== Graphical objects

Graphical objects (lines, arcs, rectangles, circles, polygons, and text) can exist on any layer but
cannot be assigned to a net.  Rectangles, circles, and polygons can be set to be filled or outlines
in their properties dialogs.  The line width property will control the width of the outline even
for filled shapes.  Line width can be set to `0` for filled shapes to disable the outline.

==== Creating graphical shapes

NOTE: TODO: Write this section

==== Creating text objects

Graphical text may be placed by using the (image:images/icons/text_24.png[]) icon in the right
toolbar or by keyboard shortcut kbd:[Ctrl+Shift+T].  Click to place the text origin, and then
edit the text and its properties in the dialog that will appear:

image::images/text_properties_dialog.png[scaledwidth="70%"]

Text may be placed on any layer, but note that text on copper layers cannot be associated with
a net and cannot form connections to tracks or pads.  Copper zones will fill around the
rectangular bounding box of text objects.

==== Board outlines (Edge Cuts)

KiCad uses graphical objects on the `Edge.Cuts` layer to define the board outline.  The outline
must be a continuous (closed) shape, but can be made up of different types of graphical object such
as lines and arcs, or be a single object such as a rectangle or polygon.  If no board outline is
defined, or the board outline is invalid, some functions such as the 3D viewer and some design rule
checks will not be functional.

=== Dimensions

Dimensions are graphical objects used to show a measurement or other marking on a board design.
They may be added on any drawing layer, but are normally added to one of the User layers.  KiCad
currently supports four different types of dimension: aligned, orthogonal, center, and leader.

**Aligned** dimensions (image:images/icons/add_aligned_dimension_24.png[]) show a measurement of
distance between two points.  The measurement axis is the line that connects those two points, and
the dimension graphics are kept parallel to that axis.

**Orthogonal** dimensions (image:images/icons/add_orthogonal_dimension_24.png[]) also measure the
distance between two points, but the measurement axis is either the X or Y axis.  In other words,
these dimensions show the horizontal or vertical component of the distance between two points.
When creating orthogonal dimensions, you can select which axis to use as the measurement axis based
on where you place the dimension after selecting the two points to measure.

**Center** dimensions (image:images/icons/add_center_dimension_24.png[]) create a cross mark to
indicate a point or the center of a circle or arc.

**Leader** dimensions (image:images/icons/add_leader_24.png[]) create an arrow with a leader line
connected to a text field.  This text field can contain any text, and an optional circular or
rectangular frame around the text.  This type of dimension is often used to call attention to parts
of the design for reference in fabrication notes.

image::images/dimensions.png[scaledwidth="70%"]

After creating a dimension, its properties may be edited (hotkey kbd:[E]) to change the format of
the displayed number and the style of the text and graphic lines.

NOTE: You may customize the default style of newly-created dimension objects in the Text & Graphics
      Defaults section of the Board Setup dialog.

image::images/dimensions_dialog.png[scaledwidth="70%"]

==== Dimension format options

**Override value:** When enabled, you may enter a measurement value directly into the **Value**
field that will be used instead of the actual measured value.

**Prefix:** Any text entered here will be shown before the measurement value.

**Suffix:** Any text entered here will be shown after the measurement value.

**Layer:** Selects which layer the dimension object exists on.

**Units:** Selects which units to display the measured value in.  **Automatic** units will result
in the dimension units changing when the display units of the board editor are changed.

**Units format:** Select from several built-in styles of unit display.

**Precision:** Select how many digits of precision to display.

==== Dimension text options

Most of the dimension text options are identical to those options available for other graphical
text objects (see the Graphical Objects section above).  Some specific options for dimension text
are also available:

**Position mode:** Choose whether to postion the dimension text manually, or to automatically keep
it aligned with the dimension measurement lines.

**Keep aligned with dimension:** When enabled, the orientation of the dimension text will be 
adjusted automatically to keep the text parallel with the measurement axis.

==== Dimension line options

**Line thickness:** Sets the thickness of the graphical lines that make up a dimension's shape.

**Extension line offset:** Sets the distance from the measurement point to the start of the
extension lines.

**Arrow length:** Sets the length of the arrow segments of the dimension's shape.

==== Leader options

image::images/dimensions_leader.png[scaledwidth="70%"]

**Value:** Enter the text to show at the end of the leader line.

**Text frame:** Select the desired border around the text (circle, rectangle, or none).

=== Routing tracks

KiCad features an interactive router that:

* Allows manual or guided (semi-automatic) routing of single tracks and differential pairs.
* Enables modifications of existing designs by:
** Re-routing existing tracks when they are dragged
** Re-routing tracks attached to footprint pads when the footprint is dragged
* Allows tuning of track lengths and differential pair skew (phase) by inserting serpentine +
 tuning shapes for designs with tight timing requirements

By default, the router respects the configured design rules when placing tracks: the size (width)
of new tracks will be taken from the design rules and the router will respect the copper clearance
set in the design rules when determining where new tracks and vias can be placed.  It is possible
to disable this behavior if desired by using the Highlight Collisions router mode and turning on
the Allow DRC Violations option in the router settings (see below).

The router has three modes that can be selected at any time.  The router mode is used for routing
new tracks, but also when dragging existing tracks using the Drag (hotkey kbd:[D]) command.  These
modes are:

- **Highlight Collisions**: in this mode, most of the router features are disabled and routing is
  fully manual.  When routing, _collisions_ (clearance violations) will be highlighted in green
  and the newly-routed tracks cannot be fixed in place if there is a collision unless the Allow
  DRC Violations option is turned on.  In this mode, up to two track segments may be placed at a
  time (for example, one horizontal and one diagonal segment).
- **Shove**: in this mode, the track being routed will walk around obstacles that cannot be moved
  (for example, pads and locked tracks/vias) and _shove_ obstacles that can be moved out of the
  way.  The router prevents DRC violations in this mode: if there is no way to route to the cursor
  position that does not violate DRC, no new tracks will be created.
- **Walk Around**: in this mode, the router behaves the same as in Shove mode, except no obstacles
  will be moved out of the way.

Which mode to use is a matter of preference.  For most users, we recommend using Shove mode for
the most efficient routing experience or Walk Around mode if you do not want the router to modify
tracks that are not being routed.  Note that Shove and Walk Around modes always create horizontal,
vertical, and 45-degree (H/V/45) track segments.  If you need to route tracks with angles other
than H/V/45, you must use Highlight Collisions mode and enable the Free Angle Mode option in the
Interactive Router Settings dialog.

There are five main routing functions: Route Single Track, Route Differential Pair, Tune length
of a single track, Tune length of a differential pair, and Tune skew of a differential pair.  All
of these are present in both the Route menu dropdown (individually) on the top toolbar and the
drawing toolbar in two overloaded icons on the drawing toolbar on the right.  The use of the
overloaded icons is described above.  One is for the two Route functions and one is for the three
Tune functions.  In addition, the Route menu allows the selection of Set Layer Pair and
Interactive Router Settings.

To route tracks, click the Route Tracks image:images/icons/add_tracks.png[] icon (from the
drawing toolbar or from the top toolbar under *Route*) or use the hotkey kbd:[X].  Click on a
starting location to select which net to route and begin routing.  The net being routed will
automatically be highlighted and the allowable clearance for the net will be indicated with a
gray outline around the tracks being routed.  The clearance outline can be disabled by changing
the Clearance Outlines setting in the Display Options section of the Preferences dialog.

NOTE: The clearance outline shows the maximum clearance from the routed net to any other copper on
      the PCB.  It is possible to use custom design rules to specify different clearances for a net
      to different objects.  These clearances will be respected by the router, but only the largest
      clearance value will be shown visually.

When the router is active, new track segments will be drawn from the routing start point to the
editor cursor.  These tracks are _unfixed_ temporary objects that show what tracks will be created
when you use a left-click or the kbd:[Enter] key to _fix_ the route.  The unfixed track segments
are shown in a brighter color than the fixed track segments.  When you exit the router using the
kbd:[Esc] key or by selecting another tool, only the fixed track segments will be saved.  The
Finish Route action (hotkey kbd:[End]) will fix all tracks and exit the router.

While you are routing, you can use the Undo Last Segment command (hotkey kbd:[Backspace]) to unfix
the tracks you most recently fixed.  You can use this command repeatedly to step back through the
route that you have already fixed.

In previous versions of KiCad, using the left mouse button or kbd:[Enter] to fix the routed
segments would fix all segments up to but _not including_ the segment ending at the mouse cursor
location.  In KiCad 6, this behavior is now optional, and by default, all segments _including_ the
one ending at the mouse cursor location will be fixed.  The old behavior can be restored by
disabling the "Fix all segments on click" option in the Interactive Router Settings dialog.

While routing, you can hold the kbd:[Ctrl] key to disable grid snapping, and hold the kbd:[Shift]
key to disable snapping to objects such as pads and vias.

NOTE: Snapping to objects can also be disabled by changing the Magnetic Points preferences in the
      Editing Options section of the Preferences dialog.  We recommend that you leave object
      snapping enabled in general, so that you do not accidentally end tracks slightly off-center
      on a pad or via.

==== Track posture

When routing in H/V/45 mode, the _posture_ refers to how a set of two track segments connect two
points that cannot be reached by a single H/V/45-degree segment.  In such a case, the points will
be connected by one horizontal or vertical segment and one diagonal (45-degree) segment.  The
posture refers to the order of these segments: whether the horizontal/vertical segment or the
diagonal segment comes first.

image:images/pcbnew_posture_a.png[width="45%"]
image:images/pcbnew_posture_b.png[width="45%"]

KiCad's router attempts to pick the best posture automatically based on a number of factors.  In
general, the router will attempt to minimize the number of corners in a route, and will avoid "bad"
corners such as acute angles whenever possible.  When routing from or to a pad, KiCad will choose
the posture that lines up the route with the longest edge of the pad.

In some cases, KiCad cannot guess the posture you intend correctly.  To switch the posture of the
track while routing, use the Switch Track Posture command (hotkey kbd:[/]).

In situations where there is no obvious "best" posture (for example, when starting a route from a
via), KiCad will use the movement of your mouse cursor to select the posture.  If you would like
the route to begin with a straight (horizontal or vertical) segment, move the mouse away from the
starting location in a mostly horizontal or vertical direction.  If you would like the route to
begin diagonally, move in a diagonal direction.  Once the cursor is a sufficient distance away from
the routing start location, the posture is "locked" and will no longer change unless the cursor is
brought back to the starting location.  Detection of posture from the movement of the mouse cursor
can be disabled in the Interactive Router Settings dialog as described below.

NOTE: If you use the Switch Track Posture command to override the posture chosen by KiCad, the
      automatic detection of posture from mouse movement will be disabled for the remainder of the
      current routing operation.

==== Track corner mode

KiCad's router can place tracks with either sharp or rounded (arc) corners when routing in H/V/45
mode.  To switch between sharp and rounded corners, use the Track Corner Mode command (hotkey
kbd:[Ctrl+/]).  When routing with rounded corners, each routing step will place either a straight
segment, a single arc, or both a straight segment and an arc.  The track posture determines whether
the arc or the straight segment will be placed first.

Track corners can also be rounded after routing by using the Fillet Tracks command after selecting
the desired tracks.

NOTE: Dragging of tracks with arcs is not yet supported.  Arcs will be converted back to sharp
      corners when dragging tracks or when tracks are moved by the router in Shove mode.

==== Track width

The width of the track being routed is determined in one of three ways: if the routing start point
is the end of an existing track and the image:images/icons/auto_track_width.png[] button on the top
toolbar is enabled, the width will be set to the width of the existing track.  Otherwise, if the
track width dropdown in the top toolbar is set to "use netclass width", the width will be taken
from the netclass of the net being routed (or from any custom design rules that specify a different
width for the net, such as inside a neckdown area).  Finally, if the track width dropdown is set to
one of the pre-defined track sizes configured in the Board Setup dialog, this width will be used.

NOTE: The track width can never be lower than the minimum track width configured in the Constraints
      section of the Board Setup dialog.  If a pre-defined width is added that is lower than this
      minimum constraint, the minimum constraint value will be used instead.

KiCad's router supports a single track width for the active route.  In other words, to change widths
in the middle of a track, you must end the route and then restart a new route from the end of the
previous route.  To change the width of the active route, use the hotkeys kbd:[W] and kbd:[Shift+W]
to step through the track widths configured in the Board Setup dialog.

==== Placing vias

While routing tracks, switching layers will insert a through via at the end of the current
(unfixed) track.  Once you place the via, routing will continue on the new layer.  There are several
ways to select a new layer and insert a via:

- By using the hotkey to select a specific layer, such as kbd:[PgUp] to select `F.Cu` or kbd:[PgDn]
  to select `B.Cu`.
- By using the "next layer" or "previous layer" hotkeys (kbd:[+] and kbd:[-]).
- By using the "Place Via" hotkey (kbd:[V]), which will switch to the next layer in the active
  layer pair.
- By using the Select Layer and Place Through Via action (hotkey kbd:[<]), which will open a dialog
  to select the target layer.

The size of the via will be taken from the active Via Size setting, accessible from the drop-down
in the top toolbar or the Increase Via Size (kbd:[']) and Decrease Via Size (kbd:[\ ]) hotkeys.
Much like track width, when the via size is set to "use netclass sizes", the via sizes configured
in the Net Classes section of the Board Setup will be used (unless overridden by a custom design
rule).

If microvias or blind/buried vias are enabled in the Constraints section of the Board Setup dialog,
these vias can be placed while routing.  Use the hotkey kbd:[Ctrl+V] to place a microvia and
kbd:[Alt+Shift+V] to place a blind/buried via.  Microvias may only be placed such that they connect
one of the outer copper layers to an adjacent layer.  Blind/buried vias may be placed on any layer.

Vias placed by the router are considered to be part of a routed track.  This means that the via net
can be updated automatically (just like track nets can), for example when updating the PCB from the
schematic changes the net name of the track.  In some cases this may not be desired, such as when
creating stitching vias.  The automatic update of via nets can be disabled for specific vias by
turning off the "automatically update via nets" checkbox in the via properties dialog.  Vias placed
with the Add Free-standing Vias tool are created with this setting disabled.

==== Routing differential pairs

Differential pairs in KiCad are defined as nets with a common _base name_ and a positive and
negative suffix.  KiCad supports using `+` and `-`, or `P` and `N` as the suffix.  For example, the
nets `USB+` and `USB-` form a differential pair, as do the nets `USB_P` and `USB_N`.  In the first
example, the base name is `USB`, and `USB_` in the second.  The suffix styles cannot be mixed: the
nets `USB+` and `USB_N` do not form a differential pair.  Make sure you name your differential pair
nets accordingly in the schematic in order to allow use of the differential pair router in the PCB
editor.

To route a differential pair, click the Route Differential Pairs
image:images/icons/ps_diff_pair.png[] icon (from the drawing toolbar or from the top toolbar under
*Route*) or use the hotkey kbd:[6].  Click on a pad, via, or the end of an existing differential
pair track to start routing.  You can start routing from either the positive or negative net of a
differential pair.

NOTE: It is not currently possible to start routing a differential pair in the middle of an
      existing differential pair track.

The differential pair router will attempt to route the pair of tracks with a gap taken from the
design rules (differential pair gap can be configured in the Net Classes section of the Board Setup
dialog, or by using custom design rules).  If the starting or ending location of the route is a
different distance apart from the configured gap, the router will create a short "fan out" section
to minimize the length of track where the differential pair is not coupled.

When switching layers or using the Place Via (kbd:[V]) action, the differential pair router will
create two vias next to each other.  These vias will be placed as close as possible to each other
while respecting the design rules for copper and hole-to-hole clearance.

==== Modifying tracks

After tracks have been routed, they can be modified by moving or dragging, or deleted and
re-routed.  When a single track segment is selected, the hotkey kbd:[U] can be used to expand the
selection to all connected track segments.  The first press of kbd:[U] will select track segments
between the nearest junctions with pads or vias.  The second press of kbd:[U] will expand the
selection again to include all track segments connected to the selected track on all layers.
Selecting tracks with this technique can be used to quickly delete an entire routed net.

There are two different drag commands that can be used to modify a track segment.  The Drag
(45-degree mode) command, hotkey kbd:[D], is used to drag tracks using the router.  If the router
mode is set to Shove, dragging with this command will shove nearby tracks.  If the router mode is
set to Walk Around, dragging with this command will walk around or stop at obstacles.  The Drag
Free Angle command, hotkey kbd:[G], is used to split a track segment into two and drag the new
corner to any location.  Drag Free Angle behaves like the Highlight Collisions router mode:
obstacles will not be avoided or shoved, only highlighted.

NOTE: Dragging of tracks containing arcs is not yet possible.  Attempting to drag these tracks will
      result in the arcs being removed in some cases.  It is possible to resize a particular arc by
      selecting it and using the drag command (kbd:[D]).  When resizing an arc using this command,
      no DRC checking is performed.

The Move command (hotkey kbd:[M]) can also be used on track segments.  This command will pick up
the selected track segments, ignoring any attached track segments or vias that are not selected.  No
DRC checking is done when moving tracks using the Move command.

It is possible to re-route tracks attached to footprints while moving the footprints.  To do so,
use the drag command (kbd:[D]) with a footprint selected.  Any tracks that end at one of the
footprint's pads will be dragged along with the footprint.  This feature has some limitations:
it only operates in Highlight Collisions mode, so the tracks attached to footprints will not walk
around obstacles or shove nearby tracks out of the way.  Additionally, only tracks that end at the
origin of the footprint's pads will be dragged.  Tracks that simply pass through the pad or that
end on the pad at a location other than the origin will not be dragged.

It is possible to modify the width of tracks and the size of vias without re-routing them using the
Edit Tracks and Vias dialog.  See the section below on bulk editing tools for details.

==== Length tuning

The length tuning tools can be used to add serpentine tuning shapes to tracks after routing.  To
tune the length of a track, first pick the appropriate length tuning tool.  The single track tuning
tool (icon image:images/icons/ps_tune_length.png[] or hotkey kbd:[7]) will add serpentine shapes
to bring the length of a single track up to the target value.  The differential pair tuning tool
(icon image:images/icons/ps_diff_pair_tune_length.png[] or hotkey kbd:[8]) will do the same for a
differential pair.  The differential pair skew tuning tool (icon
image:images/icons/ps_diff_pair_tune_phase.png[] or hotkey kbd:[9]) will add length to the
shorter member of a differential pair in order to eliminate skew (phase difference) between the
positive and negative sides of the pair.  As with the Routing icons, the Tuning icons are found
in both the Route menu dropdown from the top toolbar and the drawing toolbar on the right.

To select the target length for the length tuning tools, open the Length Tuning Settings dialog
from the context menu or with the hotkey kbd:[Ctrl+L] after activating the length tuning tool:

image::images/pcbnew_length_tuning_settings.png[]

This dialog can also be used to configure the size, shape, and spacing of the meander shapes.

After configuring the target length, click a track in the area that you wish to start placing
meander shapes.  Move the mouse cursor along the track, and meander shapes will be added.  A status
window will appear next to the cursor showing the current length of the track and the target
length.  Click again to finish placing the current meander.  Multiple meanders can be placed on the
same track if desired.

NOTE: The length tuning tools only support tuning the length of point-to-point nets between two
      pads.  Tuning the length of nets with different topologies is not yet supported.

==== Interactive router settings

The interactive router settings can be accessed through the Route menu, or by right-clicking on the
Route Tracks button in the toolbar.  These settings control the router behavior when routing tracks
as well as when dragging existing tracks.

image::images/pcbnew_interactive_router_settings.png[]

[options="header",cols="25%,75%"]
|====
| Setting | Description
| Mode    | Sets the operating mode of the router for creating new tracks and dragging existing
            tracks.  See above for more information.
| Free angle mode | Allows routing tracks at any angle, instead of just at 45-degree increments.
                    This option is only available if the router mode is set to Highlight
                    collisions.
| Jump over obstacles | In Shove mode, allows the router to attempt to move colliding tracks behind
                        solid obstacles (such as pads).
| Remove redundant tracks | Automatically removes loops created in the currently-routed track,
                            keeping only the most recently routed section of the loop.
| Optimize pad connections | When this setting is enabled, the router attempts to avoid acute
                             angles and other undesirable routing when exiting pads and vias.
| Smooth dragged segments | When dragging tracks, attempts to combine track segments together to
                            minimize direction changes.
| Allow DRC violations | In Highlight collisions mode, allows placing tracks and vias that violate
                         DRC rules.  It has no effect in other modes.
| Optimize entire track being dragged
    | When enabled, dragging a track segment will result in KiCad optimizing the rest of the track
      that is visible on the screen.  The optimization process removes unnecessary corners, avoids
      acute angles, and generally tries to find the shortest path for the track.  When disabled,
      no optimizations are performed to the track outside of the immediate section being dragged.
| Use mouse path to set track posture
    | Attempts to pick the track posture based on the mouse path from the routing start location.
| Fix all segments on click
    | When enabled, clicking while routing will fix the position of all the track segments that
      have been routed, including the segment that ends at the mouse cursor.  A new segment will
      be started from the mouse cursor location.  When disabled, the last segment (the one that
      ends at the mouse cursor) will not be fixed in place and can be adjusted by further mouse
      movement.

|====

=== Forward and back annotation

NOTE: TODO: Write this section

==== Geographical re-annotation

NOTE: TODO: Write this section

=== Locking

Most objects can be locked through their properties dialogs, by using the right-click context menu,
or by using the Toggle Lock hotkey (kbd:[L]).  Locked objects cannot be selected unless the "Locked
items" checkbox is enabled in the selection filter.  Attempting to move locked items will result in
a warning dialog:

image::images/pcbnew_locked_items_dialog.png[]

Selecting "Override Locks" in this dialog will allow moving the locked items.  Selecting "OK" will
allow you to move any unlocked items in the selection; leaving the locked items behind.  Selecting
"Do not show again" will remember your choice for the rest of your session.

=== Bulk editing tools

NOTE: TODO: Write this section

=== Cleanup tools

NOTE: TODO: Write this section

=== Importing graphics

NOTE: TODO: Write this section

==== Importing vector drawings from DXF and SVG files

NOTE: TODO: Write this section

==== Importing bitmap images

NOTE: TODO: Write this section
